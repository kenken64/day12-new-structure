/**
 * Created by phangty on 28/10/16.
 */
'use strict';

var express = require("express");
var path = require("path");
var DepartmentController = require("./api/department/department.controller");
var ManagerController = require("./api/manager/manager.controller");


const CLIENT_FOLDER = path.join(__dirname + '/../client');


module.exports = {
    init: configureRoutes,
    errorHandler: errorHandler
}

function configureRoutes(app){
    app.get("/api/departments", DepartmentController.list);
    app.get("/api/department_number/:dept_no", DepartmentController.get);
    app.put("/api/departments", DepartmentController.update);
    app.post("/api/departments", DepartmentController.create);

    app.delete("/api/department_manager/:emp_no", ManagerController.delete);
    app.get("/api/departmentManager", ManagerController.get);

    app.use(express.static(CLIENT_FOLDER));

}


function errorHandler(app) {
    app.use(function (req, res) {
        res.status(401).sendFile(CLIENT_FOLDER + "/app/errors/404.html");
    });

    app.use(function (err, req, res, next) {
        res.status(500).sendFile(path.join(CLIENT_FOLDER + '/app/errors/500.html'));
    });
};
