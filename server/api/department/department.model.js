/**
 * Created by phangty on 28/10/16.
 */
//Create a model for departments table
var Sequelize = require("sequelize");

module.exports = function (database) {
    var Department = database.define("departments", {
        dept_no: {
            type: Sequelize.STRING,
            primaryKey: true
        },
        dept_name: {
            type: Sequelize.STRING
        }
    }, {
        timestamps: false
    });
    return Department;
};