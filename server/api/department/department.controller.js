/**
 * Created by phangty on 28/10/16.
 */
var Department = require("../../database").Department;


exports.list = function (req, res){
    var where = {};

    if (req.query.dept_name) {
        where.dept_name = {
            $like: "%" + req.query.dept_name + "%"
        }
    }

    if (req.query.dept_no) {
        where.dept_no = req.query.dept_no;
    }

    console.log("where: " + JSON.stringify(where));
    Department
        .findAll({
            where: where
        })
        .then(function (departments) {
            res.json(departments);
        })
        .catch(function () {
            res
                .status(500)
                .json({error: true})
        });

};

exports.get = function(req, res){
    var where = {};

    if (req.params.dept_no) {
        where.dept_no = req.params.dept_no
    }

    console.log("where: " + JSON.stringify(where));
    Department
        .findOne({
            where: where,
            include: [
                Manager
            ]
        })
        .then(function (departments) {
            res.json(departments);
        })
        .catch(function (error) {
            console.log(error);
            res
                .status(500)
                .json({error: true})
        });
};

exports.update = function(req , res){
    console.log(req.body);

    Department.find({
        where: {
            dept_no: req.body.dept_no
        }
    })
        .then(function (response) {
            console.log('DB Response ', response);
            response.updateAttributes(
                {
                    dept_name: req.body.dept_name
                })
                .then(function (response) {
                    console.log(response);
                })
                .catch(function(error){
                    console.log(error);
                })
        })
};


exports.create = function(req, res){
    console.log("Query", req.query);
    console.log("Body", req.body);

    Department
        .create({
            dept_no: req.body.dept_no,
            dept_name: req.body.dept_name
        })
        .then(function (department) {
            res
                .status(200)
                .json(department);
        })
        .catch(function (err) {
            console.log("error: " + err);
            res
                .status(500)
                .json({error: true});
        })

};