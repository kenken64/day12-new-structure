/**
 * Created by phangty on 28/10/16.
 */
var Manager = require("../../database").Manager;



exports.get = function(req,res){
    var where = {};

    console.log("query: " + JSON.stringify(req.query));
    if (req.query.dept_name) {
        where.dept_name = {
            $like: "%" + req.query.dept_name + "%"
        }
    }

    if (req.query.dept_no) {
        where.dept_no = req.query.dept_no;
    }

    Department
        .findAll({
            where: where,
            include: [
                Manager
            ]
        })
        .then(function (departments) {
            res.json(departments);
        })
        .catch(function (err) {
            console.log("error " + err);
            res
                .status(500)
                .json({error: true})
        });
};


exports.delete= function(req, res){
    console.log('-----------', req.params.emp_no);
    Manager.findOne(
        {
            where: {
                'emp_no': req.params.emp_no
            }
        }
    ).then(function (response) {

        console.log('----response-----------', response.dataValues.from_date);
        Manager.destroy({
            where: {
                emp_no: req.params.emp_no
            }
        }).then(function (result) {
            if (result == "1")
                res.json({success: true});
            else
                res.json({success: false});

        });
    })

};