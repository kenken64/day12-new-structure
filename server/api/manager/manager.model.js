/**
 * Created by phangty on 28/10/16.
 */
var Sequelize = require("sequelize");

module.exports = function (database) {
    var Manager = database.define('manager', {
            emp_no: {
                type: Sequelize.INTEGER,
                primaryKey: true
            },
            dept_no: {
                type: Sequelize.STRING
            },
            from_date: Sequelize.DATE,
            to_date: Sequelize.DATE
        }, {
            timestamps: false,
            tableName: "dept_manager"
        }
    );
    return Manager;
};