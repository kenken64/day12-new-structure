/**
 * Created by phangty on 28/10/16.
 */
var Sequelize = require("sequelize");
var config = require('./config');

var database = new Sequelize(
    config.mysql.database,
    config.mysql.username,
    config.mysql.password, {
        host: config.mysql.host,
        dialect: 'mysql',
        pool: {
            max: 5,
            min: 0,
            idle: 10000
        }
    });

var DepartmentModel = require('./api/department/department.model')(database);
var ManagerModel = require('./api/manager/manager.model')(database);

DepartmentModel.hasOne(ManagerModel, {foreignKey: 'dept_no'});

database.sync().then(function () {
    console.log("Database in Sync Now")
});


module.exports = {
    Department: DepartmentModel,
    Manager: ManagerModel
};